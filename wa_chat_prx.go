package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type message struct {
	ID      string `json:"id"`
	TIME    string `json:"time"`
	USER    string `json:"user"`
	CONTENT string `json:"content"`
	REPLIES int    `json:"replies"`
}

func getMessages(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, messages)
}

func postMessages(c *gin.Context) {
	var newMsg message
	if err := c.BindJSON(&newMsg); err != nil {
		return
	}

	messages = append(messages, newMsg)
	c.IndentedJSON(http.StatusCreated, newMsg)
}

func getMsgByID(c *gin.Context) {
	id := c.Param("id")

	for _, a := range messages {
		if a.ID == id {
			c.IndentedJSON(http.StatusOK, a)
			return
		}
	}

	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "message not found"})
}

var messages = []message{
	{ID: "1", TIME: "3 days ago", USER: "John Coltrane", CONTENT: "WTF are you doing now? it's almost done here.", REPLIES: 0},
	{ID: "2", TIME: "3 days ago", USER: "Gerry Muller", CONTENT: "would be there at 9 sharp, get ready for everything before I get there.", REPLIES: 2},
	{ID: "3", TIME: "4 days ago", USER: "Sarah Vaughan", CONTENT: "hi, there!", REPLIES: 1},
}

func main() {
	router := gin.Default()
	router.GET("/messages", getMessages)
	router.GET("/messages/:id", getMsgByID)
	router.POST("/messages", postMessages)

	router.Run("localhost:8080")
}
